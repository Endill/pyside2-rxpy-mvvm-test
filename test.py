from rx import operators as ops

from reactive.ObservableList import ObservableList

ol = ObservableList([1, 2, 3, 4])
ol.when_collection_changes().pipe(
    ops.map(lambda x: x.Items),
).subscribe(lambda x: print(x))

ol.append(5)