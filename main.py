from __future__ import annotations

# pylint: disable=function-redefined,no-name-in-module

from abc import ABCMeta
from enum import Flag, auto
import logging
from pathlib import Path
import sys
from typing import (
    Optional, TypeVar, Type, Generic, overload, Dict, Any, TYPE_CHECKING,
    Union, Callable, List, Iterator, cast, _GenericAlias
)

from PySide2.QtCore import (  # type: ignore
    Signal, QObject, Slot, Qt, QAbstractListModel, QModelIndex, QEvent,
    QAbstractItemModel
)
from PySide2.QtGui import QPixmap, QImage
from PySide2.QtWidgets import (
    QApplication, QMainWindow, QLineEdit, QWidget, QVBoxLayout, QLabel,
    QPushButton, QComboBox, QCheckBox, QGraphicsView, QSpinBox, QHBoxLayout,
    QGraphicsPixmapItem, QGraphicsScene,
)
from rx.core.observable import Observable
from rx.core.typing import Observer, Scheduler
from rx.disposable import Disposable
import rx.operators as ops
import vapoursynth as vs
from vapoursynth import VideoNode

# issues from pyside bugtracker:
# https://bugreports.qt.io/projects/PYSIDE/issues/PYSIDE-939
# https://bugreports.qt.io/projects/PYSIDE/issues/PYSIDE-945
# https://bugreports.qt.io/projects/PYSIDE/issues/PYSIDE-1006
# https://bugreports.qt.io/projects/PYSIDE/issues/PYSIDE-1052
# https://bugreports.qt.io/projects/PYSIDE/issues/PYSIDE-1064

T = TypeVar('T')
U = TypeVar('U')


class QABCMeta(ABCMeta, type(QObject)): pass  # type: ignore
class QABC(metaclass=QABCMeta): pass
class QObservable(Observable, QObject, metaclass=QABCMeta): pass


class RxProperty(QObservable, Generic[T]):
    type_specializations: Dict[Type, Type[RxProperty]] = {}  # pylint: disable=undefined-variable
    def __class_getitem__(cls, ty: Type) -> Type[RxProperty]:
        if ty.__class__ is _GenericAlias:
            ty = ty.__args__[0]

        if ty in cls.type_specializations:
            return cls.type_specializations[ty]

        dct = {
            'ty': ty,
            '_value_changed': Signal(ty),
        }

        spec = type(cls.__name__, (cls,), dct)
        cls.type_specializations[ty] = spec
        return spec

    @property
    def name(self) -> str:
        return self._name

    @property
    def owner(self) -> Type[U]:
        return self._owner

    def __init__(self, init_value: T) -> None:
        def subscribe(observer: Observer, scheduler: Optional[Scheduler] = None) -> Disposable:
            def action(value: T) -> None:
                observer.on_next(value)
            assert self._value_changed.connect(action)

            return Disposable()

        assert hasattr(self, 'ty'), '{} requires type of underlying data to be specified'.format(type(self).__name__)

        super().__init__(subscribe)

        self._value = init_value
        self._name = ""

    def __set_name__(self, owner: Type[U], name: str) -> None:
        self._name = name
        self._owner = owner

    @overload
    def __get__(self, instance: None, owner: Type[U]) -> RxProperty: ...
    @overload
    def __get__(self, instance: U, owner: Type[U]) -> T: ...
    def __get__(self, instance, owner):  # type: ignore
        if instance is None:
            return self
        return self._value

    def __set__(self, instance: ViewModel, value: Union[T, Observable]) -> None:
        def update(value: T) -> None:
            if value == self._value:
               return
            self._value = value
            instance.property_changed.emit(self)
            self._value_changed.emit(self._value)    

        if instance is None:
            raise AttributeError

        if isinstance(value, Observable):
            value.subscribe(on_next=update)
        else:
            update(value)

    def __str__(self) -> str:
        return repr(self)

    def __repr__(self) -> str:
        return '{}[{}]({})'.format(type(self).__name__, self.ty.__name__, self._value)

class ListModel(QAbstractListModel, Generic[T]):
    __slots__ = (
        'items',
    )

    def __init__(self, init_list: Optional[List[T]] = None) -> None:
        super().__init__()
        self._items: List[T] = init_list if init_list is not None else []

    def __getitem__(self, i: int) -> T:
        return self._items[i]

    def __len__(self) -> int:
        return len(self._items)

    def index_of(self, item: T) -> int:
        return self._items.index(item)

    def __getiter__(self) -> Iterator[T]:
        return iter(self._items)

    def append(self, item: T) -> int:
        index = len(self._items)
        self.beginInsertRows(QModelIndex(), index, index)
        self._items.append(item)
        self.endInsertRows()  # type: ignore

        return len(self._items) - 1

    def clear(self) -> None:
        self.beginRemoveRows(QModelIndex(), 0, len(self._items))
        self._items.clear()
        self.endRemoveRows()  # type: ignore

    def data(self, index: QModelIndex, role: int = Qt.UserRole) -> Optional[Union[str, T]]:  # type: ignore
        if not index.isValid():
            return None
        if index.row() >= len(self._items):
            return None

        if role == Qt.DisplayRole:  # type: ignore
            return str(self._items[index.row()])
        if role == Qt.EditRole:  # type: ignore
            return str(self._items[index.row()])
        if role == Qt.UserRole:  # type: ignore
            return self._items[index.row()]
        return None

    def rowCount(self, parent: QModelIndex = QModelIndex()) -> int:
        return len(self._items)

    def flags(self, index: QModelIndex) -> int:
        if not index.isValid():
            return Qt.ItemIsEnabled  # type: ignore

        return super().flags(index) | Qt.ItemIsEditable  # type: ignore

    def setData(self, index: QModelIndex, value: Any, role: int = Qt.EditRole) -> bool:  # type: ignore
        if not index.isValid():
            return False
        if not role == Qt.EditRole:  # type: ignore
            return False

        self._items[index.row()] = value
        self.dataChanged.emit(index, index, [role])
        return True

class Output:
    def __init__(self, vs_output: VideoNode, index: int, current_frame: int = 0):
        self.index = index
        self.vs_output: VideoNode = self.prepare_vs_output(vs_output)
        self.total_frames = cast(int, self.vs_output.num_frames)

        self._current_frame = current_frame
        self.graphics_item: Optional[QGraphicsPixmapItem] = None

    def prepare_vs_output(self, vs_output: VideoNode) -> VideoNode:
        resizer = vs.core.resize.Bicubic
        resizer_kwargs = {
            'format'        : vs.COMPATBGR32,
            'matrix_in_s'   : '709',
            'transfer_in_s' : '709',
            'primaries_in_s': '709',
            'range_in_s'    : 'full',
            'chromaloc_in_s': 'left',
            'prefer_props'  : True,
        }

        vs_output = vs.core.std.FlipVertical(vs_output)

        if vs_output.format == vs.COMPATBGR32:  # type: ignore
            return vs_output

        is_subsampled = vs_output.format.subsampling_w != 0 or vs_output.format.subsampling_h != 0
        if not is_subsampled:
            resizer = vs.core.resize.Point

        if vs_output.format.color_family == vs.RGB:
            del resizer_kwargs['matrix_in_s']

        vs_output = resizer(vs_output, **resizer_kwargs)

        return vs_output

    def __getitem__(self, index: int) -> QPixmap:
        if index < 0 or index >= self.total_frames:
            raise IndexError
        return self.render_frame(index)

    def __len__(self) -> int:
        return self.total_frames

    @property
    def current_frame(self) -> int:
        return self._current_frame
    @current_frame.setter
    def current_frame(self, new_frame: int) -> None:
        if self.graphics_item is not None:
            self.graphics_item.setPixmap(self[new_frame])
        self._current_frame = new_frame

    def render_frame(self, frame: int) -> QPixmap:
        import ctypes

        vs_frame = self.vs_output.get_frame(frame)

        frame_pointer  = vs_frame.get_read_ptr(0)
        frame_stride   = vs_frame.get_stride(0)
        frame_itemsize = vs_frame.format.bytes_per_sample

        data_pointer = ctypes.cast(
            frame_pointer,
            ctypes.POINTER(ctypes.c_char * (frame_itemsize * vs_frame.width * vs_frame.height))
        )[0]
        frame_image  = QImage(data_pointer, vs_frame.width, vs_frame.height, frame_stride, QImage.Format_RGB32)
        frame_pixmap = QPixmap.fromImage(frame_image)

        return frame_pixmap

    def __str__(self) -> str:
        return f'Output {self.index}'


class ViewModel(QObject):
    property_changed = Signal(RxProperty)

class View(QMainWindow):
    class BindKind(Flag):
        SOURCE_TO_VIEW = auto()
        VIEW_TO_SOURCE = auto()
        BIDIRECTIONAL  = SOURCE_TO_VIEW | VIEW_TO_SOURCE

        @classmethod
        def is_from_source(cls, value: Any) -> bool:
            return value in (cls.SOURCE_TO_VIEW, cls.BIDIRECTIONAL)

        @classmethod
        def is_from_view(cls, value: Any) -> bool:
            return value in (cls.VIEW_TO_SOURCE, cls.BIDIRECTIONAL)

    @property
    def data_context(self) -> ViewModel:
        return self._data_context

    def __init__(self, data_context: ViewModel):
        super().__init__()
        self._data_context = data_context
        self._properties = type(self._data_context)
        self._listeners: Dict[RxProperty, List[Callable]] = {}

        ret = self.data_context.property_changed.connect(self.on_property_changed)
        assert ret

    def add_property_listener(self, prop: RxProperty, listener: Callable) -> None:
        if prop not in self._listeners:
            self._listeners[prop] = []
        self._listeners[prop].append(listener)

    def on_property_changed(self, prop: RxProperty) -> None:
        for handler in self._listeners[prop]:
            # invokeMethod() should be used here, probably
            handler()


class LineEdit(QLineEdit):
    def __init__(self, parent: View, contents: str = "") -> None:
        super().__init__(contents, parent)
        self._parent = parent
        self._data_context = parent.data_context

    def bind(self, prop: RxProperty, bind_kind: View.BindKind) -> None:
        def get_from_property() -> None:
            value = getattr(self._data_context, prop.name)
            self.setText(str(value))

        def set_to_property(new_text: str) -> None:
            value = prop.ty(new_text)
            setattr(self._data_context, prop.name, value)

        if View.BindKind.is_from_source(bind_kind):
            self._parent.add_property_listener(prop, get_from_property)
            get_from_property()
        if View.BindKind.is_from_view(bind_kind):
            ret = self.textChanged.connect(set_to_property)
            assert ret

class Label(QLabel):
    def __init__(self, parent: View) -> None:
        super().__init__(parent)
        self._parent = parent
        self._data_context = parent.data_context

    def bind(self, prop: RxProperty, bind_kind: View.BindKind) -> None:
        assert bind_kind is View.BindKind.SOURCE_TO_VIEW

        def get_from_property() -> None:
            value = getattr(self._data_context, prop.name)
            if prop.ty is QPixmap:
                self.setPixmap(value)
            else:
                self.setText(str(value))

        self._parent.add_property_listener(prop, get_from_property)
        get_from_property()
    
class PushButton(QPushButton):
    def bind(self, handler: Callable, bind_kind: View.BindKind) -> None:
        assert bind_kind is View.BindKind.VIEW_TO_SOURCE

        ret = self.clicked.connect(handler)
        assert ret

class ComboBox(QComboBox):
    def __init__(self, parent: View) -> None:
        super().__init__(parent)
        self._parent = parent
        self._data_context = parent.data_context

    def bind_current_item(self, prop: RxProperty, bind_kind: View.BindKind) -> None:
        def get_from_property() -> None:
            value = getattr(self._data_context, prop.name)
            if value is None:
                return
            index = self.model().index_of(value)
            self.setCurrentIndex(index)

        def set_to_property(new_index: int) -> None:
            new_item = self.model()[new_index]
            setattr(self._data_context, prop.name, new_item)

        if View.BindKind.is_from_source(bind_kind):
            self._parent.add_property_listener(prop, get_from_property)
        if View.BindKind.is_from_view(bind_kind):
            ret = self.currentIndexChanged[int].connect(set_to_property)
            assert ret

    def bind_current_index(self, prop: RxProperty, bind_kind: View.BindKind) -> None:
        def get_from_property() -> None:
            value = getattr(self._data_context, prop.name)
            if value is None:
                value = -1
            self.setCurrentIndex(value)

        def set_to_property(new_index: int) -> None:
            value = prop.ty(new_index)
            if value == -1:
                value = None
            setattr(self._data_context, prop.name, value)

        if View.BindKind.is_from_source(bind_kind):
            self._parent.add_property_listener(prop, get_from_property)
        if View.BindKind.is_from_view(bind_kind):
            ret = self.currentIndexChanged[int].connect(set_to_property); assert ret

    def bind_model(self, model: QAbstractItemModel, bind_kind: View.BindKind) -> None:
        assert bind_kind == View.BindKind.SOURCE_TO_VIEW

        self.setModel(model)

class CheckBox(QCheckBox):
    def __init__(self, parent: View) -> None:
        super().__init__(parent)
        self._parent = parent
        self._data_context = parent.data_context

    def bind(self, prop: RxProperty, bind_kind: View.BindKind) -> None:
        def get_from_property() -> None:
            value = getattr(self._data_context, prop.name)
            self.setChecked(bool(value))

        def set_to_property(new_state: int) -> None:
            checked = (new_state == Qt.Checked)  # type: ignore
            value = prop.ty(checked)
            setattr(self._data_context, prop.name, value)

        if View.BindKind.is_from_source(bind_kind):
            self._parent.add_property_listener(prop, get_from_property)
            get_from_property()
        if View.BindKind.is_from_view(bind_kind):
            ret = self.textChanged.connect(set_to_property)
            assert ret

class SpinBox(QSpinBox):
    def __init__(self, parent: View) -> None:
        super().__init__(parent)
        self._parent = parent
        self._data_context = parent.data_context

    def bind_value(self, prop: RxProperty, bind_kind: View.BindKind) -> None:
        def get_from_property() -> None:
            value = int(getattr(self._data_context, prop.name))
            self.setValue(value)

        def set_to_property(new_value: int) -> None:
            value = prop.ty(new_value)
            setattr(self._data_context, prop.name, value)

        if View.BindKind.is_from_source(bind_kind):
            self._parent.add_property_listener(prop, get_from_property)
            get_from_property()
        if View.BindKind.is_from_view(bind_kind):
            ret = self.valueChanged.connect(set_to_property)
            assert ret

    def bind_max_value(self, prop: RxProperty, bind_kind: View.BindKind) -> None:
        assert bind_kind == View.BindKind.SOURCE_TO_VIEW

        def get_from_property() -> None:
            value = int(getattr(self._data_context, prop.name))
            self.setMaximum(value)

        self._parent.add_property_listener(prop, get_from_property)
        get_from_property()

class GraphicsView(QGraphicsView):
    def __init__(self, parent: View, scene: GraphicsScene) -> None:
        super().__init__(parent)
        self._parent = parent
        self.setScene(scene)
        self._data_context = parent.data_context

    def bind_foreground_output(self, prop: RxProperty[Output], bind_kind: View.BindKind) -> None:
        assert bind_kind == View.BindKind.SOURCE_TO_VIEW

        def get_from_property() -> None:
            value = getattr(self._data_context, prop.name)
            if value is None:
                return
            self.scene().switch_to(value)

        self._parent.add_property_listener(prop, get_from_property)
        get_from_property()

    def bind_outputs_model(self, model: ListModel[Output], bind_kind: View.BindKind) -> None:
        assert bind_kind == View.BindKind.SOURCE_TO_VIEW

        self.scene().set_outputs_model(model)

class GraphicsScene(QGraphicsScene):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.item_in_foreground: Optional[QGraphicsPixmapItem] = None

    def set_outputs_model(self, model: ListModel[Output]) -> None:
        def add_output(output: Output) -> None:
            output.graphics_item = self.addPixmap(output.render_frame(0))
            output.graphics_item.hide()  # type: ignore

        def remove_output(output: Output) -> None:
            if output.graphics_item is None:
                return
            self.removeItem(output.graphics_item)
            output.graphics_item = None

        def on_outputs_added(index: QModelIndex, first: int, last: int) -> None:
            for i in range(first, last + 1):
                add_output(model[i])

        def on_outputs_removed(index: QModelIndex, first: int, last: int) -> None:
            if last - first + 1 == len(model):
                self.clear()  # type: ignore
                return

            for i in range(first, last + 1):
                remove_output(model[i])

        for output in model:
            add_output(output)

        ret = model.rowsInserted.connect(on_outputs_added); assert ret
        ret = model.rowsAboutToBeRemoved.connect(on_outputs_removed); assert ret
    
    def switch_to(self, output: Output) -> None:
        if self.item_in_foreground is not None:
            self.item_in_foreground.hide()  # type: ignore
        if output.graphics_item is None:
            return
        output.graphics_item.show()  # type: ignore
        self.item_in_foreground = output.graphics_item


class MainView(View):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        self.setup_ui()

        self.         lineedit.bind(self._properties.text,    View.BindKind.BIDIRECTIONAL)
        self.       text_label.bind(self._properties.text,    View.BindKind.SOURCE_TO_VIEW)
        self.     length_label.bind(self._properties.length,  View.BindKind.SOURCE_TO_VIEW)
        self.    changes_label.bind(self._properties.changes, View.BindKind.SOURCE_TO_VIEW)
        self .backspace_button.bind(self.data_context.move_last_character_to_list, View.BindKind.VIEW_TO_SOURCE)
        self.alphabet_combobox.bind_model(self.data_context.alphabet, View.BindKind.SOURCE_TO_VIEW)

    def setup_ui(self) -> None:
        self.central_widget = QWidget(self)
        self.main_layout = QVBoxLayout(self.central_widget)
        self.setCentralWidget(self.central_widget)

        self.lineedit = LineEdit(self)
        self.main_layout.addWidget(self.lineedit)

        self.text_label = Label(self)
        self.main_layout.addWidget(self.text_label)

        self.length_label = Label(self)
        self.main_layout.addWidget(self.length_label)

        self.changes_label = Label(self)
        self.main_layout.addWidget(self.changes_label)

        self.backspace_button = PushButton(self)
        self.backspace_button.setText('Backspace')
        self.main_layout.addWidget(self.backspace_button)

        self.alphabet_combobox = ComboBox(self)
        self.main_layout.addWidget(self.alphabet_combobox)

class MainViewModel(ViewModel):
    text     = RxProperty[str]('')
    length   = RxProperty[str]('0 characters')
    changes  = RxProperty[str]('0 changes')
    alphabet = ListModel[str](['a', 'b', 'c'])
    alphabet_cap = ListModel[str]([])

    def __init__(self) -> None:
        super().__init__()

        self.length = type(self).text.pipe(
            ops.map(lambda s: f'{len(s)} characters')
        )

        self.changes = type(self).text.pipe(
            ops.map_indexed(lambda _, i: f'{i + 1} changes')
        )

    def move_last_character_to_list(self) -> None:
        if len(self.text) > 0:
            self.alphabet.append(self.text[-1])
        self.text = self.text[0:-1]


class VSPMainView(View):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        self.setup_ui()

        self.graphics_view.bind_foreground_output(self._properties.current_output, View.BindKind.SOURCE_TO_VIEW)
        self.graphics_view.bind_outputs_model(self._data_context.outputs, View.BindKind.SOURCE_TO_VIEW)
        self.outputs_combobox.bind_current_item(self._properties.current_output, View.BindKind.BIDIRECTIONAL)
        self.outputs_combobox.bind_model(self._data_context.outputs, View.BindKind.SOURCE_TO_VIEW)
        self.frame_spinbox.bind_value(self._properties.current_frame, View.BindKind.BIDIRECTIONAL)
        self.frame_spinbox.bind_max_value(self._properties.last_frame, View.BindKind.SOURCE_TO_VIEW)
        self.test_button.bind(self._data_context.on_test_pressed, View.BindKind.VIEW_TO_SOURCE)

    def setup_ui(self) -> None:
        self.central_widget = QWidget(self)
        self.main_layout = QVBoxLayout(self.central_widget)
        self.setCentralWidget(self.central_widget)

        self.graphics_view = GraphicsView(self, GraphicsScene())
        self.main_layout.addWidget(self.graphics_view)

        self.toolbar_layout = QHBoxLayout()
        self.main_layout.addLayout(self.toolbar_layout)

        self.outputs_combobox = ComboBox(self)
        self.toolbar_layout.addWidget(self.outputs_combobox)

        self.frame_spinbox = SpinBox(self)
        self.toolbar_layout.addWidget(self.frame_spinbox)

        self.test_button = PushButton(self)
        self.test_button.setText('Load Script')
        self.toolbar_layout.addWidget(self.test_button)

class VSPMainViewModel(ViewModel):
    current_frame  = RxProperty[int](0)
    last_frame     = RxProperty[int](0)
    current_output = RxProperty[Optional[Output]](None)
    outputs = ListModel[Output]()

    def __init__(self) -> None:
        super().__init__()

        self.current_frame = type(self).current_output.pipe(
            ops.map(lambda output: output.current_frame if output is not None else 0)
        )
        self.last_frame = type(self).current_output.pipe(
            ops.map(lambda output: output.total_frames - 1 if output is not None else 0),
        )

        type(self).current_frame.subscribe(self.switch_frame)

    def switch_frame(self, frame: int) -> None:
        if self.current_output is None:
            return
        self.current_output.current_frame = frame

    def on_test_pressed(self) -> None:
        self.load_script(Path(r'tests\script.vpy').resolve())

    def load_script(self, path: Path) -> None:
        from traceback import print_exc

        self.outputs.clear()
        vs.clear_outputs()

        sys.path.append(str(path.parent))
        try:
            exec(path.read_text(), {})  # pylint: disable=exec-used
        except Exception:  # pylint: disable=broad-except
            print_exc()
        finally:
            sys.path.pop()

        for i, vs_output in vs.get_outputs().items():
            self.outputs.append(Output(vs_output, i))


class Application(QApplication):
    def notify(self, obj: QObject, event: QEvent) -> bool:
        isex = False
        try:
            return QApplication.notify(self, obj, event)
        except Exception:  # pylint: disable=broad-except
            isex = True
            logging.error('Application: unexpected error')
            print(*sys.exc_info())
            return False
        finally:
            if isex:
                self.quit()  # type: ignore

def main() -> None:
    import sys
    from argparse import ArgumentParser

    app = Application(sys.argv)
    view_model = VSPMainViewModel()
    view = VSPMainView(view_model)
    view.show()  # type: ignore

    try:
        app.exec_()
    except Exception:  # pylint: disable=broad-except
        logging.error('app.exec_() exception')

if __name__ == '__main__':
    main()
